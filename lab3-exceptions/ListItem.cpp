#include "ListItem.h"

#include <iostream>

ListItem::ListItem(Person* person) : next(0), person(person) {
}

ListItem::~ListItem() {
}


void ListItem::print() {
	Person* person = this->getPerson();
	std::cout << person->getName() << " " << person->getSurname() << "\t PESEL: " << person->getPesel() << std::endl;
}

