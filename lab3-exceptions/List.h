#ifndef LIST_H_
#define LIST_H_

#include "ListItem.h"
#include "Person.h"
#include <string>

class List {
private:
	int size;
	ListItem* elements;
	std::string name;

public:
	List(std::string name);
	virtual ~List();

	void add(Person* person);
	void remove(int i);
	Person* get(int i) const;
	void clean();
	void print();

	int getSize() const {
		return size;
	}

	const std::string& getName() const {
		return name;
	}

	void setName(const std::string& name) {
		this->name = name;
	}
};

#endif /* LIST_H_ */
