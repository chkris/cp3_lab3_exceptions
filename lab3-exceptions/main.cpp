//============================================================================
// Name        : main.cpp
// Author      : YourNameHere
// Version     : 1
// Copyright   : 2012
// Description : Exceptions in C++, Ansi-style
//============================================================================

#include <iostream>
using namespace std;

#include "Person.h"
#include "List.h"

int main() {

	List* myList = new List("myList");

	//Add some persons
	myList->add(new Person("Jan","Kowalski","000000000"));
	myList->add(new Person("Pawel","Nowak","000000000"));
	myList->add(new Person("Adrian","Koder","000000000"));
	myList->add(new Person("Piotr","Ziolko","000000000"));
	myList->print();

	//remove single person
	myList->remove(3);
	myList->print();

	//get single person
	Person* p = myList->get(1);
	cout << p->getName() << std::endl<< std::endl;

	//clean list
	myList->clean();
	myList->print();

	return 0;
}